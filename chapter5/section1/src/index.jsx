import {observable, action, observe, toJS, trace, spy} from 'mobx';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

// spy
spy((event) => {
    if (event.type === 'action') {
        console.log(`${event.name} with args: ${event.arguments}`)
    }
})

class Store {
    @observable todos = [];

    constructor() {
        try {
            let todos = localStorage.getItem('todos');
            if ('string' === typeof todos) {
                todos = JSON.parse(todos);
                if (Array.isArray(todos)) {
                    this.todos.push(...todos);
                }
            }
        } catch(e) {

        }
        // observe
        observe(this.todos, (change) => {
            // toJS
            localStorage.setItem('todos', JSON.stringify(toJS(change.object)))
        });
    }

    @action add(title) {
        this.todos.push({
            id: parseInt(Math.random() * 1e7, 10),
            title,
            timestamp: Date.now()
        });
    }

    @action remove(id) {
        const todo = this.todos.find(todo => todo.id === id);
        this.todos.remove(todo);
    }
}

const store = new Store();

@observer
class TodoItem extends Component {
    static propTypes = {
        finishTodo: PropTypes.func
    };
    render() {
        const todo = this.props.todo;
        return <li><span>{todo.title}</span> <span onClick={() => this.props.finishTodo(todo.id)}>❌</span></li>;
    }
}

@observer
class TodoList extends Component {
    state = {value: ''};
    handleSubmit = (event) => {
        event.preventDefault();
        store.add(this.state.value);

        this.setState({value: ''});
    }

    handleChange = (event) => {
        this.setState({
            value: event.target.value
        });
    }

    finishTodo = (id) => {
        store.remove(id);
    }

    render() {
        // trace(true);
        const todos = store.todos;
        return (
        <div className="todo-list">
            <form className="input" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="input a todo" value={this.state.value} onChange={this.handleChange}/>
                <button type="submit">Add</button>
            </form>
            <ul>
            {todos.map((todo) => {
                return <TodoItem key={todo.id} className="todo" todo={todo} finishTodo={this.finishTodo}/>;
            })}
            </ul>
        </div>
            );
    }
}

ReactDOM.render(<TodoList/>, document.querySelector('#root'));

export {store};