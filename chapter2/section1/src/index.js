/*function Animal() {}
function Dog() {}

Object.defineProperties(Animal.prototype, {
    name: {
        value() {
            return 'Animal';
        }
    },
    say: {
        value() {
            return `I'm ${this.name()}`;
        }
    }
});*/

// Dog->Animal,

// new Dog().__proto__...__proto__...__proto__ = Animal.prototype
// new Dog().__proto__ = Dog.prototype
// Dog.prototype.__proto__ == Animal.prototype

/*Dog.prototype = Object.create(Animal.prototype, {
    name: {
        value() {
            return 'Dog';
        }
    }
});*/

class Animal {
    name() {
        return 'Animal';
    }
    say() {
        return `I'm ${this.name()}`;
    }
}

class Dog extends Animal {
    food = 'bone';// add later
    name() {
        return 'Dog';
    }
}

var animal = new Animal;
var dog = new Dog;

console.log('animal:', animal.say());
console.log('dog:', dog.say());