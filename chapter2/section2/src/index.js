function validate(target, key, descriptor) {
    const func = descriptor.value;
    descriptor.value = function(...args) {
        for (let num of args) {
            if ('number' !== typeof num) {
                throw new Error(`"${num}" should be a number, but a ` + typeof num);
            }
        }

        return func.apply(this, args);
    };
}

function readonly(target, key, descriptor) {
    descriptor.writable = false;
}

function log(target) {
    const desc = Object.getOwnPropertyDescriptors(target.prototype);
    for (let key of Object.keys(desc)) {
        if (key === 'constructor') {
            continue;
        }
        let func = desc[key].value;
        if ('function' === typeof func) {
            Object.defineProperty(target.prototype, key, {
                value(...args) {
                    console.log('before ' + key);
                    let ret = func.apply(this, args);
                    console.log('after ' + key);
                    return ret;
                }
            });
        }
    }
}

@log
class Numberic {
    @readonly PI = Math.PI;

    @validate
    add(...nums) {
        return nums.reduce((p, n) => (p + n), 0)
    }
}

/*new Numberic().add(1, 2);*/
/*new Numberic().PI = 100;*/
new Numberic().add(1, 'x');