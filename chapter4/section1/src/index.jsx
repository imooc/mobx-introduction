import {observable, action} from 'mobx';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, PropTypes as ObservablePropTypes} from 'mobx-react';
import PropTypes from 'prop-types';

/*class Store {
    @observable cache = {queue: []};

    @action.bound refresh() {
        this.cache.queue.push(1);
    }
}

const store = new Store();

@observer
class Bar extends Component {
    static propTypes = {
        queue: ObservablePropTypes.observableArray
    };
    render() {
        const queue = this.props.queue;
        return <span>{queue.length}</span>;
    }
}

@observer
class Foo extends Component {
    static propTypes = {
        cache: ObservablePropTypes.observableObject,
        refresh: PropTypes.func
    };
    render() {
        const cache = this.props.cache;
        return <div><button onClick={this.props.refresh}>Press</button><Bar queue={cache.queue}/></div>;
    }
}

ReactDOM.render(<Foo cache={store.cache} refresh={store.refresh}/>, document.querySelector('#root'));*/

// 在创建 React 组件之前，我们先声明一个普通的类，用于存储数据
class Store {
    // 在Store内部我们声明一个 cache 属性，它是一个对象，并具有一个数组类型的queue属性
    @observable cache = {queue: []};

    @action.bound refresh() {
        this.cache.queue.push(1);
    }
}

// 然后我们简单new一个Store出来。
const store = new Store();

// 首先编写Bar
@observer
class Bar extends Component {
    // 编写React组件去声明属性的类型是个好习惯，在开发环
    // 境下它会对于不符合类型的属性输入报错，有利于更早发现问题
    static propTypes = {
        queue: ObservablePropTypes.observableArray
    };
    render() {
        const queue = this.props.queue;
        return <span>{queue.length}</span>;
    }
}


class Foo extends Component {
    static propTypes = {
        cache: ObservablePropTypes.observableObject
    };
    shouldComponentUpdate(){
        console.log('hahah')
        return true;
    }
    render() {
        const cache = this.props.cache;
        return <div><button onClick={this.props.refresh}>Refresh</button><Bar queue={cache.queue}/></div>;
    }
}

ReactDOM.render(<Foo cache={store.cache} refresh={store.refresh}/>, document.querySelector('#root'));