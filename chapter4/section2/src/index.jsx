import {observable, action, computed} from 'mobx';
import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';
import {observer, PropTypes as ObservablePropTypes} from 'mobx-react';
import PropTypes from 'prop-types';

class Todo {
    id = Math.random();

    @observable title = '';

    @observable done = false;

    constructor(title) {
        this.title = title;
    }

    @action.bound toggle() {
        this.done = !this.done;
    }
}

class Store {
    @observable todos = [];

    @action.bound createTodo(title) {
        this.todos.unshift(new Todo(title));
    }

    @action.bound removeTodo(todo) {
        this.todos.remove(todo);
    }

    @computed get left() {
        return this.todos.filter(todo => !todo.done).length;
    }
}

var store = new Store()


@observer
class TodoItem extends Component {
    static propTypes = {
        todo: PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            done: PropTypes.bool.isRequired
        }).isRequired
    };
    handleChange = () => {
        this.props.todo.toggle();
    }
    render() {
        const todo = this.props.todo;
        return <Fragment>
        <input type="checkbox" className="toggle" checked={todo.done} onChange={this.handleChange}/>
        <span className={["title", todo.done && 'done'].join(' ')}>{todo.title}</span></Fragment>;
    }
}

@observer
class TodoList extends Component {
    static propTypes = {
        store: PropTypes.shape({
            createTodo: PropTypes.func.isRequired,
            removeTodo: PropTypes.func.isRequired,
            todos: ObservablePropTypes.observableArrayOf(ObservablePropTypes.observableObject).isRequired
        }).isRequired
    };
    state = {inputValue: ''};
    handleSubmit = e => {
        e.preventDefault();
        this.props.store.createTodo(this.state.inputValue);

        this.setState({
            inputValue: ''
        })
    }

    handleChange = e => {
        this.setState({
            inputValue: e.target.value
        });
    }

    render() {
        const store = this.props.store;
        const todos = store.todos;
        return <div className="todo-list">
            <header>
            <form onSubmit={this.handleSubmit}>
                <input className="input" type="text" value={this.state.inputValue} placeholder="What needs to be done?" onChange={this.handleChange}/>
            </form>
            </header>
            <ul>
            {todos.map(todo => {
                return <li key={todo.id} className="todo-item"><TodoItem todo={todo}/><span className="delete" onClick={e=>store.removeTodo(todo)}>❌</span></li>;
            })}
            </ul>
            <footer>{store.left} item(s) undone</footer>
        </div>;
    }
}

ReactDOM.render(<TodoList store={store}/>, document.getElementById('root'));