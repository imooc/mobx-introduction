import {observable, computed, autorun, when, reaction, action} from 'mobx';

class Foo {
    @observable string = 'string';
    @observable number = 5;
    @observable bool = false;
    @observable symbol = Symbol('foo');
    @observable array = [];
    @observable obj = {};
    @observable map = new Map();

    @computed get bar() {
        return this.number + '/' + this.string;
    }

    @action modify(str, num) {
        this.string = str;
        this.number = num;
    }
}

const foo = new Foo();

// autorun(function() {
//     console.log(this.string);
// });
// foo.string = 'str';

// when(
//     () => foo.bool,
//     () => console.log(foo.bool)
// );
// foo.bool = true;

// useStrict(true);

reaction(
    () => foo.bar,
    bar => console.log(bar)
);

foo.modify('str', 6);

export {Foo};