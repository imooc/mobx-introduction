import {observable} from 'mobx';

// string
let string = observable.box('string');
console.log(string);
console.log(string.get());

// boolean
let btrue = observable.box(true);
console.log(btrue);
console.log(btrue.get());

// symbol
let symbol = observable.box(Symbol('bar'));
console.log(symbol);
console.log(symbol.get());

// number
let number = observable.box(56);
console.log(number);
console.log(number.get());

// array
let array = observable([1,2,3]);
console.log(array);

// object
let obj = observable({a: 1});
console.log(obj);

// map
let map = observable(new Map([['a', 1]]));// observable.map({a: 1})
console.log(map);

class Foo {
    @observable string = 'string';
    @observable number = 5;
    @observable bool = false;
    @observable symbol = Symbol('foo');
    @observable array = [];
    @observable obj = {};
    @observable map = new Map();
}

export {Foo};