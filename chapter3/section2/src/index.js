import {observable, computed, autorun, when, reaction} from 'mobx';

class Foo {
    @observable string = 'string';
    @observable number = 5;
    @observable bool = false;
    @observable symbol = Symbol('foo');
    @observable array = [];
    @observable obj = {};
    @observable map = new Map();

    @computed get bar() {
        return this.number + '/' + this.string;
    }
}

const foo = new Foo();

console.log(foo.bar)

// var f = computed(() => foo.string + '/' +  foo.number).observe(change => console.log(change));
// foo.string = 'x';
// foo.number = 6;

// autorun(function() {
//     console.log(this.string);
// });
// foo.string = 'str';

// when(
//     () => foo.bool,
//     () => console.log(foo.bool)
// );
// foo.bool = true;

// reaction(
//     () => foo.bar,
//     bar => console.log(bar)
// );
// foo.number = 6;

export {Foo};